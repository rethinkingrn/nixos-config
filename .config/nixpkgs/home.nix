{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "matthew";
  home.homeDirectory = "/home/matthew";
 

programs.zsh = {
  enable = true;
  shellAliases = {
    vim = "nvim";
    la = "ls -a";
    update = "sudo nixos-rebuild switch";
    updatehome = "home-manager switch";
  };
 zplug = {
    enable = true;
    plugins = [
      { name = "zsh-users/zsh-autosuggestions"; } # Simple plugin installation
      { name = "zsh-users/zsh-syntax-highlighting"; } 
    ];
  }; 

  history = {
    size = 25000;
    path = "/home/matthew/zsh/history";
  };
};

 programs.starship = {
    enable = true;
    enableZshIntegration = true;
    # Configuration written to ~/.config/starship.toml
    settings = {
       add_newline = true;
       character = {
         success_symbol = "[❯](bold green)[❯](bold #61afef)[❯](bold #c678dd)";
         error_symbol = "[❯❯❯](bold red)";
       };
      # package.disabled = true;
    };
  };

home.packages = with pkgs; [ 
  neofetch
  lutris
  runelite
  spotify 
  papirus-icon-theme
  xfce.thunar
  xfce.xfconf
  xfce.tumbler
  xfce.exo 
];
  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";
}
